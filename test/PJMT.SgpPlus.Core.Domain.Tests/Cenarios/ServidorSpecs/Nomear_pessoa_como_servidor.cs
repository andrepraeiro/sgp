﻿using System;
using System.Collections.Generic;
using System.Linq;
using EnjoyCQRS.Events;
using EnjoyCQRS.TestFramework;
using FluentAssertions;
using PJMT.SgpPlus.Core.Domain.Aggregates;
using PJMT.SgpPlus.Core.Domain.Aggregates.Events.Servidor;
using PJMT.SgpPlus.Core.Domain.ValueObjects;
using Xunit;

namespace PJMT.SgpPlus.Core.Domain.Tests.Cenarios.ServidorSpecs
{
    public class Nomear_pessoa_como_servidor : AggregateTestFixture<Servidor>
    {

        private Guid _idServidor;
        private Guid _idPessoa;

        protected override IEnumerable<IDomainEvent> Given()
        {
            _idServidor = Guid.NewGuid();
            _idPessoa = Guid.NewGuid();

            yield return PrepareDomainEvent.Set(new ServidorNomeado(Guid.NewGuid(), new MovimentoNomeacao(_idPessoa, DateTime.Now, new Matricula(1), Guid.NewGuid()))).ToVersion(1);
        }

        protected override void When()
        {
            AggregateRoot.Movimentar(_idServidor, new MovimentoNomeacao(_idPessoa, DateTime.Now, new Matricula(3), Guid.NewGuid()));
        }

        [Fact]
        public void Deve_conter_um_novo_movimento_de_nomeacao()
        {
            AggregateRoot.Movimentacao.Should().HaveCount(2);
        }

        [Fact]
        public void Deve_lancar_um_evento_de_servidor_nomeado()
        {
            PublishedEvents.Last().Should().BeOfType<ServidorNomeado>();
        }
    }
}
