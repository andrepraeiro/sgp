﻿using System;
using System.Linq;
using EnjoyCQRS.TestFramework;
using FluentAssertions;
using PJMT.SgpPlus.Core.Domain.Aggregates;
using PJMT.SgpPlus.Core.Domain.Aggregates.Events.Pessoa;
using Xunit;

namespace PJMT.SgpPlus.Core.Domain.Tests.Cenarios.PessoaSpecs
{
    public class Quando_uma_pessoa_é_criada : AggregateTestFixture<Pessoa>
    {
        private Guid _id;
        private string _nome;
        private string _cpf;
        private DateTime _dataNascimento;

        protected override void When()
        {
            _id = Guid.NewGuid();
            _nome = "João da Silva Sauro";
            _cpf = "12345678909";
            _dataNascimento = DateTime.Now.AddYears(20);

            AggregateRoot = new Pessoa(_id, _nome, _cpf, _dataNascimento);
        }

        [Fact]
        public void Deve_constar_o_evento_de_criacao()
        {
            PublishedEvents.Last().Should().BeOfType<PessoaCriada>();
        }

        [Fact]
        public void O_evento_PessoaCriada_deve_estar_preenchido_corretamente()
        {
            var evento = PublishedEvents.Last().As<PessoaCriada>();

            evento.AggregateId.Should().Be(_id);
            evento.Nome.Should().Be(_nome);
            evento.Cpf.Should().Be(_cpf);
            evento.DataNascimento.Should().Be(_dataNascimento);
        }
    }
}
