﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EnjoyCQRS.Events;
using EnjoyCQRS.TestFramework;
using FluentAssertions;
using PJMT.SgpPlus.Core.Domain.Aggregates;
using PJMT.SgpPlus.Core.Domain.Aggregates.Events.Pessoa;
using Xunit;

namespace PJMT.SgpPlus.Core.Domain.Tests.Cenarios.PessoaSpecs
{
    public class Quando_o_cpf_é_alterado : AggregateTestFixture<Aggregates.Pessoa>
    {
        private string _novoCpf;

        private static readonly PessoaCriada PessoaCriadaComSucesso = new PessoaCriada(Guid.NewGuid(), "João da Silva Sauro", "12345678909", new DateTime(1981, 6, 11));

        protected override IEnumerable<IDomainEvent> Given()
        {
            yield return PrepareDomainEvent.Set(PessoaCriadaComSucesso).ToVersion(1);
        }

        protected override void When()
        {
            _novoCpf = "46250742239";
            AggregateRoot.AlterarCpf(_novoCpf);
        }

        [Fact]
        public void Deve_lancar_evento_CpfAlterado()
        {
            PublishedEvents.Last().Should().BeOfType<CpfAlterado>();
        }
    }
}
