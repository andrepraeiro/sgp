﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PJMT.SgpPlus.Core.Domain.ReadModel.Entities;
using PJMT.SgpPlus.Core.Domain.ReadModel.Repositories;
using PJMT.SgpPlus.Core.Infrastructure.EntityFramework.Contexts.SGP;

namespace PJMT.SgpPlus.Core.Infrastructure.EntityFramework.Repositories
{
    public class PessoaRepository : Repository<Pessoa>, IPessoaRepository
    {
        public PessoaRepository(SgpDbContext context) : base(context)
        {
        }

        public async Task<IEnumerable<Pessoa>> GetByCpfAsync(string cpf)
        {
            return await Context.Pessoa.Where(x => x.Cpf == cpf).ToListAsync();
        }

        public async Task<IEnumerable<Pessoa>> GetAllAsync()
        {
            return await Context.Pessoa.ToListAsync();
        }
    }
}
