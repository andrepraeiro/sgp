﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using PJMT.SgpPlus.Core.Domain.ReadModel.Entities;

namespace PJMT.SgpPlus.Core.Infrastructure.EntityFramework.Contexts.SGP.Mappings
{
    class ServidorMap : EntityTypeConfiguration<Servidor>
    {
        public ServidorMap()
        {
            ToTable("Servidor", "RM");

            HasKey(x => x.Id);

            Property(x => x.Id)
                .HasColumnName("Id")
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(x => x.IdPessoa)
                .HasColumnName("IdPessoa");

            Property(x => x.AggregateId)
                .HasColumnName("AggregateId");

            Property(x => x.IdCargoAtual)
                .HasColumnName("IdCargoAtual");

            Property(x => x.DataCriacao)
                .HasColumnName("DataCriacao");

            Property(x => x.Matricula)
                .HasColumnName("Matricula");

            Property(x => x.DataNomeacao)
                .HasColumnName("DataNomeacao");

            HasRequired(x => x.Cargo)
                .WithMany()
                .HasForeignKey(x => x.IdCargoAtual);

            HasRequired(x => x.Pessoa)
                .WithMany(x => x.Servidores)
                .HasForeignKey(x => x.IdPessoa);
        }
    }
}