﻿using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using PJMT.SgpPlus.Core.Domain.ReadModel.Entities;

namespace PJMT.SgpPlus.Core.Infrastructure.EntityFramework.Contexts.SGP
{
    public class SgpDbContext : DbContext
    {
        public DbSet<Pessoa> Pessoa { get; set; }

        public DbSet<Cargo> Cargo { get; set; }

        public DbSet<Servidor> Servidor { get; set; }


        static SgpDbContext()
        {
            Database.SetInitializer<SgpDbContext>(null);
        }

        public SgpDbContext(string nameOrConnectionString) : base(nameOrConnectionString)
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<ForeignKeyNavigationPropertyAttributeConvention>();
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

            AddMappingsDynamically(modelBuilder);
        }

        private void AddMappingsDynamically(DbModelBuilder modelBuilder)
        {
            // inclui, automaticamente, as classes de mapeamento
            var currentAssembly = typeof(SgpDbContext).Assembly;
            var efMappingTypes = currentAssembly.GetTypes().Where(t =>
                t.FullName.StartsWith("PJMT.SgpPlus.Core.Infrastructure.EntityFramework.Contexts.SGP.Mappings.") &&
                t.FullName.EndsWith("Map"));

            foreach (var map in efMappingTypes.Select(Activator.CreateInstance))
            {
                modelBuilder.Configurations.Add((dynamic)map);
            }
        }
    }
}
