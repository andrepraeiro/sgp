﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using EnjoyCQRS.Commands;

namespace PJMT.SgpPlus.Core.Application.Servidor
{
    public class NomearPessoaCommand : Command
    {
        public Guid IdPessoa { get; }
        public Guid IdCargo { get; }

        public NomearPessoaCommand(Guid idPessoa, Guid idCargo) : base(idPessoa)
        {
            IdPessoa = idPessoa;
            IdCargo = idCargo;
        }
    }
}
