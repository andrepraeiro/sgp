﻿using System.Threading.Tasks;
using EnjoyCQRS.Commands;
using PJMT.SgpPlus.Core.Domain.Services;

namespace PJMT.SgpPlus.Core.Application.Servidor
{
    public class NomearPessoaCommandHandler : ICommandHandler<NomearPessoaCommand>
    {
        private readonly INomeacaoService _nomeacaoService;

        public NomearPessoaCommandHandler(INomeacaoService nomeacaoService)
        {
            _nomeacaoService = nomeacaoService;
        }

        public async Task ExecuteAsync(NomearPessoaCommand command)
        {
            await _nomeacaoService.Nomear(command.IdPessoa, command.IdCargo);
        }
    }
}