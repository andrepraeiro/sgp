﻿using System;
using EnjoyCQRS.Commands;

namespace PJMT.SgpPlus.Core.Application.Pessoa
{
    public class AlterarCpfCommand : Command
    {
        public string NovoCpf { get; }

        public AlterarCpfCommand(Guid aggregateId, string novoCpf) : base(aggregateId)
        {
            NovoCpf = novoCpf;
        }
    }
}