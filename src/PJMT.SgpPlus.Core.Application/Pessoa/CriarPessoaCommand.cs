﻿using System;
using EnjoyCQRS.Commands;

namespace PJMT.SgpPlus.Core.Application.Pessoa
{
    public class CriarPessoaCommand : Command
    {
        public string Nome { get; }
        public string Cpf { get; }
        public DateTime DataNascimento { get; }

        public CriarPessoaCommand(Guid aggregateId, string nome, string cpf, DateTime dataNascimento) : base(aggregateId)
        {
            Nome = nome;
            Cpf = cpf;
            DataNascimento = dataNascimento;
        }
    }
}
