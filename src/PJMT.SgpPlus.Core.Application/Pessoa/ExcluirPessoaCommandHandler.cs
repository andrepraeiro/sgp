﻿using System.Threading.Tasks;
using EnjoyCQRS.Commands;
using EnjoyCQRS.EventSource.Exceptions;
using EnjoyCQRS.EventSource.Storage;

namespace PJMT.SgpPlus.Core.Application.Pessoa
{
    public class ExcluirPessoaCommandHandler : ICommandHandler<ExcluirPessoaCommand>
    {
        private readonly IRepository _repository;

        public ExcluirPessoaCommandHandler(IRepository repository)
        {
            _repository = repository;
        }

        public async Task ExecuteAsync(ExcluirPessoaCommand command)
        {
            var pessoa = await _repository.GetByIdAsync<Domain.Aggregates.Pessoa>(command.AggregateId);
            if (pessoa == null) throw new AggregateNotFoundException(nameof(Domain.Aggregates.Pessoa), command.AggregateId);

            pessoa.Excluir();
        }
    }
}