﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EnjoyCQRS.Commands;
using EnjoyCQRS.EventSource.Storage;

namespace PJMT.SgpPlus.Core.Application.Cargo
{
    public class CriarCargoCommandHandler : ICommandHandler<CriarCargoCommand>
    {
        private readonly IRepository _repository;

        public CriarCargoCommandHandler(IRepository repository)
        {
            _repository = repository;
        }

        public async Task ExecuteAsync(CriarCargoCommand command)
        {
            var cargo = new Domain.Aggregates.Cargo(Guid.NewGuid(), command.Nome, command.Quantidade);
            await _repository.AddAsync(cargo);
        }
    }
}
