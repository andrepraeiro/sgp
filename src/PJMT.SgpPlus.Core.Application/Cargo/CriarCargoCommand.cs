﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EnjoyCQRS.Commands;

namespace PJMT.SgpPlus.Core.Application.Cargo
{
    public class CriarCargoCommand : Command
    {
        public string Nome { get; }
        public int Quantidade { get; }

        public CriarCargoCommand(Guid aggregateId, string nome, int quantidade) : base(aggregateId)
        {
            if (string.IsNullOrEmpty(nome)) throw new ArgumentNullException(nameof(nome));
            if (quantidade <= 0) throw new ArgumentOutOfRangeException(nameof(quantidade), quantidade, "Quantidade deve ser maior que zero.");

            Nome = nome;
            Quantidade = quantidade;
        }
    }
}
