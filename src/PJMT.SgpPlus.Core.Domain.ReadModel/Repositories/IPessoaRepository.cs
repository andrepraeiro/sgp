﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PJMT.SgpPlus.Core.Domain.ReadModel.Entities;
using PJMT.SgpPlus.Core.Infrastructure.Data;

namespace PJMT.SgpPlus.Core.Domain.ReadModel.Repositories
{
    public interface IPessoaRepository : IRepository<Pessoa>
    {
        Task<IEnumerable<Pessoa>> GetByCpfAsync(string cpf);

        Task<IEnumerable<Pessoa>> GetAllAsync();
    }
}
