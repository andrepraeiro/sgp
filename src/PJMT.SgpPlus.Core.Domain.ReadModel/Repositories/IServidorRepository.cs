﻿using System.Collections.Generic;
using System.Threading.Tasks;
using PJMT.SgpPlus.Core.Domain.ReadModel.Entities;
using PJMT.SgpPlus.Core.Infrastructure.Data;

namespace PJMT.SgpPlus.Core.Domain.ReadModel.Repositories
{
    public interface IServidorRepository : IRepository<Servidor>
    {
    }
}