﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PJMT.SgpPlus.Core.Infrastructure.Data;

namespace PJMT.SgpPlus.Core.Domain.ReadModel.Entities
{
    public class Cargo : IEntity
    {
        public int Id { get; set; }
        public Guid AggregateId { get; set; }
        public DateTime DataCriacao { get; set; }

        public string Nome { get; set; }

        public int Quantidade { get; set; }
        
    }
}
