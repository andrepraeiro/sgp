﻿using System;
using PJMT.SgpPlus.Core.Infrastructure.Data;

namespace PJMT.SgpPlus.Core.Domain.ReadModel.Entities
{
    public class Servidor : IEntity
    {
        public int Id { get; set; }

        public Guid AggregateId { get; set; }

        public DateTime DataCriacao { get; set; }

        public int IdPessoa { get; set; }

        public int IdCargoAtual { get; set; }

        public int Matricula { get; set; }

        public DateTime DataNomeacao { get; set; }

        public virtual Cargo Cargo { get; set; }

        public virtual Pessoa Pessoa { get; set; }
    }
}