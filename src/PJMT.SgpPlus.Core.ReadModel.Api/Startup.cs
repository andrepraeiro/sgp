﻿using System.Web.Http;
using Owin;

namespace PJMT.SgpPlus.Core.ReadModel.Api
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var config = new HttpConfiguration();

            CorsConfig.Configure(app);

            AutofacConfig.Configure(app, config);

            WebApiConfig.Configure(config);

            SwaggerConfig.Configure(config);

            app.UseWebApi(config);
        }
    }
}