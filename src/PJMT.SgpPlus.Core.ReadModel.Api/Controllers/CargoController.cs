﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using PJMT.SgpPlus.Core.Domain.ReadModel.Entities;
using PJMT.SgpPlus.Core.Domain.ReadModel.Repositories;

namespace PJMT.SgpPlus.Core.ReadModel.Api.Controllers
{
    [RoutePrefix("v1/cargos")]
    public class CargoController : ApiController
    {
        private readonly ICargoRepository _cargoRepository;

        public CargoController(ICargoRepository cargoRepository)
        {
            _cargoRepository = cargoRepository;
        }

        [Route("{id:int}")]
        public async Task<Cargo> GetByIdAsync(int id)
        {
            return await _cargoRepository.GetByIdAsync(id);
        }

        [Route("")]
        public async Task<IEnumerable<Cargo>> GetAllAsync()
        {
            return await _cargoRepository.GetAllAsync();
        }

        [Route("")]
        public async Task<Cargo> GetByIdAsync(Guid aggregateId)
        {
            return await _cargoRepository.GetByAggregateIdAsync(aggregateId);
        }

    }
}
