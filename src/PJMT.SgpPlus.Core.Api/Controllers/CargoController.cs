﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using EnjoyCQRS.EventSource;
using EnjoyCQRS.Messages;
using PJMT.SgpPlus.Core.Api.Models;
using PJMT.SgpPlus.Core.Application.Cargo;

namespace PJMT.SgpPlus.Core.Api.Controllers
{
    [RoutePrefix("v1/cargos")]
    public class CargoController : ApiController
    {
        private readonly ICommandDispatcher _dispatcher;
        private readonly IUnitOfWork _unitOfWork;

        public CargoController(ICommandDispatcher dispatcher)
        {
            _dispatcher = dispatcher;
        }

        [Route("")]
        [HttpPost]
        public async Task<Guid> Criar(CriarCargoDto dto)
        {
            var id = Guid.NewGuid();
            var command = new CriarCargoCommand(id, dto.Nome, dto.Quantidade);

            await _dispatcher.DispatchAsync(command);

            return id;
        }
    }
}