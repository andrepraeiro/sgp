﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using EnjoyCQRS.Messages;
using PJMT.SgpPlus.Core.Api.Models;
using PJMT.SgpPlus.Core.Application.Pessoa;

namespace PJMT.SgpPlus.Core.Api.Controllers
{
    [RoutePrefix("v1/pessoas")]
    public class PessoaController : ApiController
    {
        private readonly ICommandDispatcher _dispatcher;

        public PessoaController(ICommandDispatcher dispatcher)
        {
            _dispatcher = dispatcher;
        }

        [Route("")]
        [HttpPost]
        public async Task<Guid> Criar(CriarPessoaDto dto)
        {
            var id = Guid.NewGuid();
            var command = new CriarPessoaCommand(id, dto.Nome, dto.Cpf, dto.DataNascimento);

            await _dispatcher.DispatchAsync(command);

            return id;
        }

        [Route("alterar-cpf")]
        [HttpPost]
        public async Task AlterarCpf(AlterarCpfPessoaDto pessoaDto)
        {
            var command = new AlterarCpfCommand(pessoaDto.IdPessoa, pessoaDto.NovoCpf);

            await _dispatcher.DispatchAsync(command);
        }

        [HttpDelete]
        [Route("{id:guid}")]
        public async Task ExcluirPessoa(Guid id)
        {
            var command = new ExcluirPessoaCommand(id);

            await _dispatcher.DispatchAsync(command);
        }
    }
}
