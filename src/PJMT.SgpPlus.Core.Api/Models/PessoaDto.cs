﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PJMT.SgpPlus.Core.Api.Models
{
    public class PessoaDto
    {
        public int Id { get; set; }

        public string Nome { get; set; }

        public string Cpf { get; set; }

        public DateTime DataNascimento { get; set; }
    }
}