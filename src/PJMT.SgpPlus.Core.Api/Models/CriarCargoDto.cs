﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PJMT.SgpPlus.Core.Api.Models
{
    public class CriarCargoDto
    {
        public string Nome { get; set; }
        public int Quantidade { get; set; }
    }
}