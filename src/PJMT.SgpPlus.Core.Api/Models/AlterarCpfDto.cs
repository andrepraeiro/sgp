﻿using System;

namespace PJMT.SgpPlus.Core.Api.Models
{
    public class AlterarCpfPessoaDto
    {
        public Guid IdPessoa { get; set; }

        public string NovoCpf { get; set; }
    }
}