﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Http;
using Swashbuckle.Application;

namespace PJMT.SgpPlus.Core.Api
{
    public static class SwaggerConfig
    {
        public static void Configure(HttpConfiguration config)
        {
            config
                .EnableSwagger(c =>
                {
                    // Registra a lista dos arquivos de comentários (documentação) dos assemblies
                    // para exibir no Swagger.
                    var binPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "bin");
                    foreach (var file in Directory.GetFiles(binPath, "PJMT.SgpPlus.*.xml"))
                    {
                        c.IncludeXmlComments(file);
                    }

                    c.SingleApiVersion("v1", "Api v1 SGP Plus");
                })
                .EnableSwaggerUi();
        }
    }
}