﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;
using EnjoyCQRS.Bus;
using EnjoyCQRS.Commands;
using EnjoyCQRS.Events;
using EnjoyCQRS.EventSource;
using EnjoyCQRS.EventSource.Storage;
using Owin;
using PJMT.SgpPlus.Core.Application;
using PJMT.SgpPlus.Core.Infrastructure.SqlServerEventStore;
using System.Linq;
using System.Threading.Tasks;
using Autofac.Builder;
using Autofac.Core;
using Autofac.Features.Scanning;
using EnjoyCQRS.Bus.InProcess;
using EnjoyCQRS.Messages;
using PJMT.SgpPlus.Core.Domain.Services;
using PJMT.SgpPlus.Core.Infrastructure.EntityFramework.Contexts.SGP;

namespace PJMT.SgpPlus.Core.Api
{
    public class AutofacConfig
    {
        internal static void Configure(IAppBuilder app, HttpConfiguration config)
        {
            var builder = new ContainerBuilder();

            // Registra os Controllers (Api's).
            builder
                .RegisterApiControllers(typeof(Startup).Assembly)
                .InstancePerRequest();

            RegisterDependencies(builder);

            var container = builder.Build();

            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);

            app.UseAutofacMiddleware(container);
            app.UseAutofacWebApi(config);
        }

        private static void RegisterDependencies(ContainerBuilder builder)
        {
            RegisterEnjoyCQRSDependencies(builder);

            builder.Register(c =>
            {
                var connectionString = ConfigurationManager.ConnectionStrings["SGP"]?.ConnectionString;
                if (string.IsNullOrEmpty(connectionString)) throw new ArgumentNullException("nameOrConnectionString", "Não foi possível obter a ConenctionString para SgpDbContext.");
                return new SgpDbContext(connectionString);
            }).AsSelf().InstancePerRequest();

            builder.RegisterAssemblyTypes(typeof(Infrastructure.EntityFramework.Foo).Assembly)
                .Where(x => x.FullName.StartsWith("PJMT.SgpPlus.Core.Infrastructure.EntityFramework.Repositories.") &&
                            x.FullName.EndsWith("Repository"))
                .AsImplementedInterfaces();

            builder.RegisterType<AnalisadorDeRequisitosService>().As<IAnalisadorDeRequisitosService>();
            builder.RegisterType<GeradorMatricula>().As<IGeradorMatricula>();
            builder.RegisterType<OcupacaoCargoService>().As<IOcupacaoCargoService>();
            builder.RegisterType<NomeacaoService>().As<INomeacaoService>();
        }

        private static void RegisterEnjoyCQRSDependencies(ContainerBuilder builder)
        {
            builder.RegisterType<AutofacCommandDispatcher>().As<ICommandDispatcher>().InstancePerLifetimeScope();
            builder.RegisterType<EventPublisher>().As<IEventPublisher>().InstancePerLifetimeScope();

            builder.RegisterType<Repository>().As<IRepository>();
            builder.RegisterType<Session>().As<ISession>().InstancePerRequest();
            
            builder.RegisterType<AutofacEventRouter>().As<IEventRouter>();

            var genericCommandHandler = typeof(ICommandHandler<>);

            builder.RegisterAssemblyTypes(typeof(Application.Foo).Assembly)
                    .AsNamedClosedTypesOf(genericCommandHandler, t => "uowCmdHandler");

            var genericEventHandler = typeof(IEventHandler<>);

            builder.RegisterAssemblyTypes(typeof(EventHandlers.Foo).Assembly)
                   .AsClosedTypesOf(genericEventHandler)
                   .AsImplementedInterfaces();

            builder.RegisterType<ComposedUnitOfWork>().As<IUnitOfWork>().InstancePerRequest();

            builder.RegisterGenericDecorator(
                typeof(TransactionalCommandHandler<>),
                genericCommandHandler,
                fromKey: "uowCmdHandler");

            builder.Register(c => new EventStore(ConfigurationManager.ConnectionStrings["SGP"].ConnectionString)).As<IEventStore>();
        }
    }

    public class ComposedUnitOfWork : IUnitOfWork
    {
        private readonly ISession _session;
        private readonly SgpDbContext _sgpDbContext;

        public ComposedUnitOfWork(ISession session, SgpDbContext sgpDbContext)
        {
            _session = session;
            _sgpDbContext = sgpDbContext;
        }

        public async Task CommitAsync()
        {
            _session.BeginTransaction();

            try
            {
                await _session.SaveChangesAsync().ConfigureAwait(false);
            }
            catch (Exception)
            {
                _session.Rollback();
                throw;
            }

            var transaction = _sgpDbContext.Database.BeginTransaction();

            try
            {
                await _sgpDbContext.SaveChangesAsync().ConfigureAwait(false);
            }
            catch (Exception)
            {
                _session.Rollback();
                transaction.Rollback();
                throw;
            }

            await _session.CommitAsync().ConfigureAwait(false);

            transaction.Commit();
        }
    }

    public class AutofacCommandDispatcher : ICommandDispatcher
    {
        private readonly ILifetimeScope _scope;

        public AutofacCommandDispatcher(ILifetimeScope scope)
        {
            _scope = scope;
        }
        
        public async Task DispatchAsync<TCommand>(TCommand command) where TCommand : ICommand
        {
            await RouteAsync((dynamic) command);
        }

        private async Task RouteAsync<TCommand>(TCommand command) where TCommand : ICommand
        {
            var handler = _scope.Resolve<ICommandHandler<TCommand>>();
            await handler.ExecuteAsync(command);
        }
    }

    public class AutofacEventRouter : IEventRouter
    {
        private readonly ILifetimeScope _scope;

        public AutofacEventRouter(ILifetimeScope scope)
        {
            _scope = scope;
        }
        
        public async Task RouteAsync<TEvent>(TEvent @event) where TEvent : IDomainEvent
        {
            var handlers = _scope.ResolveOptional<IEnumerable<IEventHandler<TEvent>>>();

            foreach (var handler in handlers)
            {
                await handler.ExecuteAsync(@event);
            }
        }
    }

    public static class CustomRegistrationExtensions
    {
        // This is the important custom bit: Registering a named service during scanning.
        public static IRegistrationBuilder<TLimit, TScanningActivatorData, TRegistrationStyle>
            AsNamedClosedTypesOf<TLimit, TScanningActivatorData, TRegistrationStyle>(
            this IRegistrationBuilder<TLimit, TScanningActivatorData, TRegistrationStyle> registration,
            Type openGenericServiceType,
            Func<Type, object> keyFactory)
            where TScanningActivatorData : ScanningActivatorData
        {
            if (openGenericServiceType == null) throw new ArgumentNullException(nameof(openGenericServiceType));

            return registration
                .Where(candidateType => candidateType.IsClosedTypeOf(openGenericServiceType))
                .As(candidateType => candidateType.GetTypesThatClose(openGenericServiceType).Select(t => (Service)new KeyedService(keyFactory(t), t)));
        }

        // These next two methods are basically copy/paste of some Autofac internals that
        // are used to determine closed generic types during scanning.
        public static IEnumerable<Type> GetTypesThatClose(this Type candidateType, Type openGenericServiceType)
        {
            return candidateType.GetInterfaces().Concat(TraverseAcross(candidateType, t => t.BaseType)).Where(t => t.IsClosedTypeOf(openGenericServiceType));
        }

        public static IEnumerable<T> TraverseAcross<T>(T first, Func<T, T> next) where T : class
        {
            var item = first;
            while (item != null)
            {
                yield return item;
                item = next(item);
            }
        }
    }
}