﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PJMT.SgpPlus.Core.Infrastructure.Data
{
    public interface IEntity
    {
        int Id { get; set; }

        Guid AggregateId { get; set; }

        DateTime DataCriacao { get; set; }
    }
}
