﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PJMT.SgpPlus.Core.Infrastructure.Data
{
    public interface IRepository<TEntity>
        where TEntity : IEntity
    {
        Task<TEntity> GetByIdAsync(int id);

        Task<TEntity> GetByAggregateIdAsync(Guid aggregateId);

        void Add(TEntity entity);

        void Remove(TEntity entity);
    }
}
