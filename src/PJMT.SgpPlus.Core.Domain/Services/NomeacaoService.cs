﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EnjoyCQRS.EventSource.Exceptions;
using EnjoyCQRS.EventSource.Storage;
using PJMT.SgpPlus.Core.Domain.Aggregates;
using PJMT.SgpPlus.Core.Domain.ReadModel.Repositories;

namespace PJMT.SgpPlus.Core.Domain.Services
{
    public class NomeacaoService : INomeacaoService
    {
        private readonly IRepository _repository;
        private readonly IGeradorMatricula _geradorMatricula;
        private readonly IAnalisadorDeRequisitosService _analisadorDeRequisitosService;
        private readonly IOcupacaoCargoService _ocupacaoCargoService;
        private readonly IPessoaRepository _pessoaRepository;

        public NomeacaoService(
            IRepository repository, 
            IGeradorMatricula geradorMatricula, 
            IAnalisadorDeRequisitosService analisadorDeRequisitosService,
            IOcupacaoCargoService ocupacaoCargoService,
            IPessoaRepository pessoaRepository
            )
        {
            _repository = repository;
            _geradorMatricula = geradorMatricula;
            _analisadorDeRequisitosService = analisadorDeRequisitosService;
            _ocupacaoCargoService = ocupacaoCargoService;
            _pessoaRepository = pessoaRepository;
        }

        public async Task Nomear(Guid idPessoa, Guid idCargo)
        {
            var pessoa = await _repository.GetByIdAsync<Pessoa>(idPessoa).ConfigureAwait(false);
            if (pessoa == null)
                throw new AggregateNotFoundException(nameof(Pessoa), idPessoa);

            var cargo = await _repository.GetByIdAsync<Cargo>(idCargo).ConfigureAwait(false);
            if (cargo == null)
                throw new AggregateNotFoundException(nameof(cargo), idCargo);

            if (await ServidorPossuiNomeacaoEmAberto(idPessoa))
                throw new ApplicationException("O Servidor possui uma Nomeação em aberto.");

            if (_ocupacaoCargoService.ObterQuantidadeDisponiveisDoCargo(idCargo) == 0)
                throw new ApplicationException("Não há vagas disponíveis para o Cargo.");

            if (!_analisadorDeRequisitosService.AtendeOsRequisitos(idPessoa, idCargo))
                throw new ApplicationException("Não atendo aos requisitos do cargo.");

            var matricula = _geradorMatricula.Gerar(idPessoa);

            var pessoaRead = await _pessoaRepository.GetByAggregateIdAsync(idPessoa);
            var movimentoNomeacao = new MovimentoNomeacao(pessoa.Id, DateTime.Now, matricula, idCargo);

            Servidor servidor;
            if (!pessoaRead.Servidores.Any())
            {
                servidor = new Servidor(Guid.NewGuid(), movimentoNomeacao);
            }
            else
            {
                var idServidor = pessoaRead.Servidores.First().AggregateId;
                servidor = await  _repository.GetByIdAsync<Servidor>(idServidor);
                servidor.Movimentar(idServidor, movimentoNomeacao);
            }

            await _repository.AddAsync(servidor);
        }

        private async Task<bool> ServidorPossuiNomeacaoEmAberto(Guid idPessoa)
        {
            var pessoa = await _pessoaRepository.GetByAggregateIdAsync(idPessoa);
            try
            {
                if (!(pessoa?.Servidores?.Any() ?? false))
                    return false;
            }
            catch (Exception ex)
            {

                throw;
            }

            var idServidor = pessoa.Servidores.First().AggregateId;

            var servidorAggr = await _repository.GetByIdAsync<Servidor>(idServidor);
            return servidorAggr != null && servidorAggr.Movimentacao.OfType<MovimentoNomeacao>().Any(m => !m.Encerrada);
        }
    }
}
