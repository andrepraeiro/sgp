using System;
using PJMT.SgpPlus.Core.Domain.ValueObjects;

namespace PJMT.SgpPlus.Core.Domain.Services
{
    public interface IGeradorMatricula
    {
        Matricula Gerar(Guid idPessoa);
    }

    public class GeradorMatricula : IGeradorMatricula
    {
        readonly Random _random = new Random();

        public Matricula Gerar(Guid idPessoa)
        {
            return _random.Next(10000, 99999);
        }
    }
}