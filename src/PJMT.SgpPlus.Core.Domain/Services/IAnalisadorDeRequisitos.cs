﻿using System;

namespace PJMT.SgpPlus.Core.Domain.Services
{
    public interface IAnalisadorDeRequisitosService
    {
        bool AtendeOsRequisitos(Guid idPessoa, Guid idCargo);
    }

    public class AnalisadorDeRequisitosService : IAnalisadorDeRequisitosService
    {
        public bool AtendeOsRequisitos(Guid idPessoa, Guid idCargo)
        {
            return true; // ~d(o_O)b~
        }
    }
}