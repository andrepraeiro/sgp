﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PJMT.SgpPlus.Core.Domain.Aggregates.Events.Lotacao
{
    public enum EEntrancia
    {
        PrimeiraEntrancia,
        SegundaEntrancia,
        TerceiraEntrancia,
        EntranciaEspecial,
        TribunalJustica
    }
}
