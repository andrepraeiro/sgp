﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PJMT.SgpPlus.Core.Domain.Aggregates.Events.Lotacao
{
    public enum ETipoLotacao
    {
        ExtraJudicial,
        TribunalJustica,
        JuizadoEspecial,
        ForoAdministrativo,
        ForoJudicialEscrivaninhas,
        PrimeiraInstacia,
        VaraEspecialInfanciaJuventude,
        GabineteJuizesSubstitutosSegundoGrau,
        GabineteDesembargadores,
        ForoJudicial       
    }
}
