﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EnjoyCQRS.Events;

namespace PJMT.SgpPlus.Core.Domain.Aggregates.Events.Lotacao
{
    public class LotacaoCriada: DomainEvent
    {
        public string Nome { get; }
        
        public LotacaoCriada(Guid aggregateId, string nome) : base(aggregateId)
        {
            Nome = nome;
        }
    }
}
