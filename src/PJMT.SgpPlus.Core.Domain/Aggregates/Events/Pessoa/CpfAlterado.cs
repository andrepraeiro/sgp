﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EnjoyCQRS.Events;

namespace PJMT.SgpPlus.Core.Domain.Aggregates.Events.Pessoa
{
    public sealed class CpfAlterado : DomainEvent
    {
        public string NovoCpf { get; }

        public CpfAlterado(Guid aggregateId, string novoCpf) : base(aggregateId)
        {
            NovoCpf = novoCpf;
        }
    }
}
