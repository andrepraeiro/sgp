using System;
using PJMT.SgpPlus.Core.Domain.ValueObjects;

namespace PJMT.SgpPlus.Core.Domain.Aggregates
{
    public class MovimentoNomeacao : Movimento
    {
        public DateTime DataNomeacao { get; }

        public Matricula Matricula { get; }

        public Guid IdCargo { get; }

        public DateTime? DataEncerramento { get; private set; }

        public bool Encerrada => DataEncerramento != null;

        public void Encerrar(DateTime dataEncerramento)
        {
            if (dataEncerramento <= DataNomeacao)
                throw new ArgumentOutOfRangeException();

            DataEncerramento = dataEncerramento;
        }

        public MovimentoNomeacao(Guid idPessoa, DateTime dataNomeacao, Matricula matricula, Guid idCargo) 
            : base(idPessoa)
        {
            DataNomeacao = dataNomeacao;
            Matricula = matricula;
            IdCargo = idCargo;
        }
    }
}