using System;
using EnjoyCQRS.EventSource;
using PJMT.SgpPlus.Core.Domain.Aggregates.Events.Cargo;

namespace PJMT.SgpPlus.Core.Domain.Aggregates
{
    public class Cargo : Aggregate
    {
        public string Nome { get; private set; }
        public int Quantidade { get; private set; }

        public Cargo()
        {
        }

        public Cargo(Guid id, string nome, int quantidade)
        {
            if (string.IsNullOrEmpty(nome)) throw new ArgumentNullException(nameof(nome));
            if (quantidade <= 0) throw new ArgumentOutOfRangeException(nameof(quantidade), quantidade, "Quantidade deve ser maior que zero.");

            Emit(new CargoCriado(id, nome, quantidade));
        }

        protected override void RegisterEvents()
        {
            SubscribeTo<CargoCriado>(Aplicar);
        }

        private void Aplicar(CargoCriado evento)
        {
            Id = evento.Id;
            Nome = evento.Nome;
            Quantidade = evento.Quantidade;
        }
    }
}