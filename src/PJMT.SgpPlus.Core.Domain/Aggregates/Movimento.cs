﻿using System;

namespace PJMT.SgpPlus.Core.Domain.Aggregates
{
    public abstract class Movimento
    {
        public Guid IdPessoa { get; }

        protected Movimento(Guid idPessoa)
        {
            IdPessoa = idPessoa;
        }
    }
}