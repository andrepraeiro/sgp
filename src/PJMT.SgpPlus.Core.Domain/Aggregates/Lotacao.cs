﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EnjoyCQRS.EventSource;
using PJMT.SgpPlus.Core.Domain.Aggregates.Events.Lotacao;

namespace PJMT.SgpPlus.Core.Domain.Aggregates
{
    public class Lotacao: Aggregate
    {
        public string Nome { get; private set; }
        public EInstancia Instancia { get; private set; }
        public EEntrancia Entrancia { get; private set; }
        public ETipoLotacao TipoLotacao { get; private set; }
        public string Lei { get; private set; }
        public DateTime DataVigencia { get; private set; }


        public Lotacao()
        {
            
        }

        public Lotacao(Guid id, string nome)
        {
            if (string.IsNullOrEmpty(nome)) throw new ArgumentNullException(nameof(nome));
            Emit(new LotacaoCriada(id, nome));
        }

        protected override void RegisterEvents()
        {
            SubscribeTo<LotacaoCriada>(Aplicar);
        }

        private void Aplicar(LotacaoCriada evento)
        {
            Id = evento.Id;
            Nome = evento.Nome;
        }        
    }
}
