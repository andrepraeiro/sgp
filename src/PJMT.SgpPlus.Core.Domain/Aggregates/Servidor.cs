﻿using System;
using System.Collections.Generic;
using EnjoyCQRS.EventSource;
using PJMT.SgpPlus.Core.Domain.Aggregates.Events.Servidor;

namespace PJMT.SgpPlus.Core.Domain.Aggregates
{
    public class Servidor : Aggregate
    {
        private readonly List<Movimento> _movimentos = new List<Movimento>();

        public IEnumerable<Movimento> Movimentacao => _movimentos.AsReadOnly();

        public Servidor()
        {
        }

        public Servidor(Guid id, MovimentoNomeacao nomeacao)
        {
            Movimentar(id, nomeacao);
        }

        protected override void RegisterEvents()
        {
            SubscribeTo<ServidorNomeado>(Aplicar);
        }

        private void Aplicar(ServidorNomeado e)
        {
            Id = e.AggregateId;
            _movimentos.Add(e.Nomeacao);
        }

        public void Movimentar(Guid id, Movimento movimento)
        {
            var nomeacao = movimento as MovimentoNomeacao;
            if (nomeacao != null)
            {
                Emit(new ServidorNomeado(id, nomeacao));
                return;
            }

            // Outras opções vão aqui...

        }
    }
}
