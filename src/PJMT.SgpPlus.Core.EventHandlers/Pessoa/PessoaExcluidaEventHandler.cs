﻿using System;
using System.Threading.Tasks;
using EnjoyCQRS.Events;
using PJMT.SgpPlus.Core.Domain.Aggregates.Events.Pessoa;
using PJMT.SgpPlus.Core.Domain.ReadModel.Repositories;

namespace PJMT.SgpPlus.Core.EventHandlers.Pessoa
{
    public class PessoaExcluidaEventHandler : IEventHandler<PessoaExcluida>
    {
        private readonly IPessoaRepository _pessoaRepository;

        public PessoaExcluidaEventHandler(IPessoaRepository pessoaRepository)
        {
            _pessoaRepository = pessoaRepository;
        }

        public async Task ExecuteAsync(PessoaExcluida @event)
        {
            Domain.ReadModel.Entities.Pessoa pessoa = await _pessoaRepository.GetByAggregateIdAsync(@event.AggregateId);

            if (pessoa == null) return;

            _pessoaRepository.Remove(pessoa);
        }
    }
}