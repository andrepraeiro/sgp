﻿using System;
using System.Threading.Tasks;
using EnjoyCQRS.Events;
using PJMT.SgpPlus.Core.Domain.Aggregates.Events.Servidor;
using PJMT.SgpPlus.Core.Domain.ReadModel.Repositories;

namespace PJMT.SgpPlus.Core.EventHandlers.Servidor
{
    public class ServidorNomeadoEventHandler : IEventHandler<ServidorNomeado>
    {
        private readonly IServidorRepository _servidorRepository;
        private readonly ICargoRepository _cargoRepository;
        private readonly IPessoaRepository _pessoaRepository;

        public ServidorNomeadoEventHandler(
            IServidorRepository servidorRepository, 
            ICargoRepository cargoRepository,
            IPessoaRepository pessoaRepository)
        {
            _servidorRepository = servidorRepository;
            _cargoRepository = cargoRepository;
            _pessoaRepository = pessoaRepository;
        }

        public async Task ExecuteAsync(ServidorNomeado @event)
        {
            var cargo = await _cargoRepository.GetByAggregateIdAsync(@event.Nomeacao.IdCargo);
            var servidor = await _servidorRepository.GetByAggregateIdAsync(@event.AggregateId);

            if (servidor == null)
            {
                var pessoa = await _pessoaRepository.GetByAggregateIdAsync(@event.Nomeacao.IdPessoa);
                if (pessoa == null)
                    throw new ApplicationException("Pessoa não cadastrada.");

                servidor = new Domain.ReadModel.Entities.Servidor 
                {
                    IdPessoa = pessoa.Id
                };

                _servidorRepository.Add(servidor);
            }

            servidor.AggregateId = @event.AggregateId;
            servidor.DataCriacao = DateTime.Now;
            servidor.IdCargoAtual = cargo.Id;
            servidor.Matricula = @event.Nomeacao.Matricula;
            servidor.DataNomeacao = @event.Nomeacao.DataNomeacao;
        }
    }
}
