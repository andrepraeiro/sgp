﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EnjoyCQRS.Events;
using PJMT.SgpPlus.Core.Domain.Aggregates.Events.Cargo;
using PJMT.SgpPlus.Core.Domain.Aggregates.Events.Pessoa;
using PJMT.SgpPlus.Core.Domain.ReadModel.Repositories;

namespace PJMT.SgpPlus.Core.EventHandlers.Cargo
{
    public class CargoCriadoEventHandler : IEventHandler<CargoCriado>
    {
        private readonly ICargoRepository _cargoRepository;

        public CargoCriadoEventHandler(ICargoRepository cargoRepository)
        {
            _cargoRepository = cargoRepository;
        }

        public async Task ExecuteAsync(CargoCriado e)
        {
            var cargo = new Domain.ReadModel.Entities.Cargo
            {
                AggregateId = e.AggregateId,
                DataCriacao = DateTime.Now,
                Nome = e.Nome,
                Quantidade = e.Quantidade
            };

            _cargoRepository.Add(cargo);
        }
    }
}
