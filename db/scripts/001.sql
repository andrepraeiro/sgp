BEGIN TRANSACTION
GO

/* EXCLUS�O */

/*
DROP TABLE rm.Pessoa
DROP TABLE rm.Cargo
DROP TABLE rm.Nomeacao
DROP TABLE wm.[Event]

DROP SCHEMA [rm]
DROP SCHEMA [wm]
GO
*/

/* CRIA��O */


CREATE SCHEMA [RM]
GO

CREATE SCHEMA [WM]
GO


CREATE TABLE WM.[Event] (
	[Id] [uniqueidentifier] NOT NULL,
	[Timestamp] [datetime] NOT NULL,
	[EventType] [varchar](100) NOT NULL,
	[AggregateId] [uniqueidentifier] NOT NULL,
	[Version] [int] NOT NULL,
	[EventData] [nvarchar](max) NOT NULL,
	[Metadata] [nvarchar](max) NULL,
    CONSTRAINT [PK_Event] PRIMARY KEY (	[Id] ASC )
)
GO


CREATE TABLE RM.[Pessoa] (
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AggregateId] [uniqueidentifier] NOT NULL,
	[DataCriacao] [datetime] NOT NULL,
	[Nome] [varchar](50) NOT NULL,
	[CPF] [char](11) NOT NULL,
	[DataNascimento] [date] NOT NULL,
    CONSTRAINT [PK_Pessoa] PRIMARY KEY ( [Id] ASC )
)
GO

CREATE TABLE RM.[Cargo] (
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AggregateId] [uniqueidentifier] NOT NULL,
	[DataCriacao] [datetime] NOT NULL,
	[Nome] [varchar](50) NOT NULL,
	[Quantidade] [int] NOT NULL,
    CONSTRAINT [PK_Cargo] PRIMARY KEY ( [Id] ASC )
)
GO

CREATE TABLE [RM].[Servidor](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdPessoa] [int] NOT NULL,
	[AggregateId] [uniqueidentifier] NOT NULL,
	[IdCargoAtual] [int] NOT NULL,
	[DataCriacao] [datetime] NOT NULL,
	[Matricula] [int] NOT NULL,
	[DataNomeacao] [date] NOT NULL,
	CONSTRAINT [PK_Servidor] PRIMARY KEY ([Id] ASC)
)
GO

ALTER TABLE [RM].[Servidor]  WITH CHECK ADD  CONSTRAINT [FK_Servidor_Cargo] FOREIGN KEY([IdCargoAtual])
REFERENCES [RM].[Cargo] ([Id])
GO

ALTER TABLE [RM].[Servidor] CHECK CONSTRAINT [FK_Servidor_Cargo]
GO

ALTER TABLE [RM].[Servidor]  WITH CHECK ADD  CONSTRAINT [FK_Servidor_Pessoa] FOREIGN KEY([IdPessoa])
REFERENCES [RM].[Pessoa] ([Id])
GO

ALTER TABLE [RM].[Servidor] CHECK CONSTRAINT [FK_Servidor_Pessoa]
GO


IF @@ERROR = 0 
BEGIN
	COMMIT TRANSACTION
END
ELSE
BEGIN
	
	ROLLBACK TRANSACTION
END


